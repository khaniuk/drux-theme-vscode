<h1 align="center">
  Drux Theme
</h1>

<p>
  Drux dark and light theme for <a href="https://code.visualstudio.com/">Visual Studio Code</a> 
</p>

## About theme

It is based on Bearded Theme and default Dark and Light Modern themes.

## Installation

- Open **Extensions**, View > Extensions
- Search for `Drux Theme`
- Click **Install**
- Select theme, File > Preferences > Theme > Color Theme > **Drux Dark Theme** or **Drux Light Theme**

## Demo

![Dark](https://gitlab.com/khaniuk/drux-theme-vscode/-/raw/main/images/drux-dark-theme.png)

![Light](https://gitlab.com/khaniuk/drux-theme-vscode/-/raw/main/images/drux-light-theme.png)

# Create you custom theme

## Generate theme

Install Yeoman and code generator

```bash
npm install -g yo generator-code
```

Generate project for new vscode theme

```bash
yo code
```

Select type of extension do you want to create

```
 > New Color theme
```

Type the data requested by the wizard options.

## Install VS Code Extension

This dependency is required to generate the packaging

```bash
npm install -g vsce
```

## Generate file to publish

```
vsce package
```

## Publish

Go to the visual studio code marketplace

- [Marketplace Visual Studio Code](https://marketplace.visualstudio.com)

- Option: **Publish extensions**
- Create or sign in with you account
- Upload the packaged file, por example **my-theme-1.0.0.vsix**

**Enjoy!**
